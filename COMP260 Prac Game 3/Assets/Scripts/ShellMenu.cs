﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShellMenu : MonoBehaviour {

	public GameObject shellPanel;
	private bool paused = true;

	void Start () {
		SetPaused(paused);
	}

	void Update () {
		// pause if the player presses escape
		if (!paused && Input.GetKeyDown(KeyCode.Escape)) {
			SetPaused(true);
		}
	}

	private void SetPaused(bool p) {
		// make the shell panel (in)active when (un)paused
		paused = p;
		shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
	}

	public void OnPressedPlay() {
		// resume the game
		SetPaused(false);
	}

	public void OnPressedQuit() {
		// quit the game
		Application.Quit();
	}

}
