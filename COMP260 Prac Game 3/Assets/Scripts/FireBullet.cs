﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	public static float reloadTime = 0.5f;
	public float timer = reloadTime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// do not run if the game is paused
		if (Time.timeScale == 0) {
			return;
		}

		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1") && timer >= reloadTime) {

			BulletMove bullet = Instantiate(bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;

			timer = 0;
		}

		timer += Time.deltaTime;
	}
}
