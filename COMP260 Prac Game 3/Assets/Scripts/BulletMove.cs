﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	// separate speed and direction so we can 
	// tune the speed without changing the code
	public float speed = 25.0f;
	public float lifeTime = 5.0f;
	private float timer = 0.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>(); 
	}

	void FixedUpdate () {
		rigidbody.velocity = speed * direction;
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}

	// Update is called once per frame
	void Update () {
		if (timer > lifeTime) { // Destroy the bullet once it has exceeded its lifetime
			Destroy(gameObject);
		}
		timer += Time.deltaTime;
	}
}
